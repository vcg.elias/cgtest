<?php
class Database{
 
    // specify your own database credentials
    private $host = "192.168.2.26:3306";
    private $db_name = "dev";
    private $username = "dev";
    private $password = "dev123";
    public $conn;
 
    // get the database connection
    public function getConnection(){
 
        $this->conn = null;
 
    
            $this->conn = new mysqli($this->host, $this->username, $this->password, $this->db_name);
    
 
        return $this->conn;
    }
}
?>