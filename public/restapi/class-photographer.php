<?php
class Photographer {
 
    // database connection and table name
    private $conn;
    private $table_name = "photographers";
 
    // object properties
    public $id;
    public $name;
    public $phone;
    public $email;
    public $bio;
    public $picture;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


    // read photographers
    function read(){
    
        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . " p
                ORDER BY
                    p.name ASC";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }
}
?>