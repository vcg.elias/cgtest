$(document).ready(function () {

    $.getJSON( "../landscapes.json", function( data ) {
        var album = data.album;
        console.log(data);
        var content = (album.map(getPhotoItem)).join(" ");

        document.getElementById("name").innerHTML = data.name;
        document.getElementById("bio").innerHTML = data.bio;
        document.getElementById("phone").innerHTML = data.phone;
        document.getElementById("email").innerHTML = data.email;

        document.getElementById("album-gallery").innerHTML = content;
    });

    function getPhotoItem (item, index) {
        var photo = item.img;
        var featured = " ";
        if ( item.featured ) {
            featured = '<i class="fa fa-heart"></i>';
        }
        var i = '<div class="col-lg-4 col-md-6 col-xs-12">' +
            '<div class="card">' +
                '<a href="../' + photo.replace("jpg", "jpeg") + '">' +
                    '<img class="card-img-top" src="../' + photo.replace("jpg", "jpeg") + '" alt="">' +
                '</a>' +
                '<div class="card-body">' +
                    '<h3 class="card-title"><a href="#url">' + item.title + '</a></h2>' +
                    '<p class="card-text">' + item.description + '</p>' +
                '</div>' +
                '<div class="card-footer">' +
                    '<small class="text-muted">' + featured + ' ' + item.date + '</small>' +
                '</div>' +               
            '</div>' +
        '</div>';

        return i;
    }

});