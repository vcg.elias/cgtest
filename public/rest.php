<?php
// required headers
//header("Access-Control-Allow-Origin: *");
//header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once 'restapi/conn.php';
include_once 'restapi/class-photographer.php';
 
// instantiate database 
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$photog = new Photographer($db);
 
// query photographers
$stmt = $photog->read();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // products array
    $photog_arr=array();
    $photog_arr["album"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $photog_item=array(
            "name" => $name,
            "phone" => $phone,
            "email" => $email,
            "bio" => html_entity_decode($bio),
            "profile_picture" => $picture
        );
 
        array_push($photog_arr, $photog_item);
    }
 
    // set response code - 200 OK
    //http_response_code(200);
 
    // show products data in json format
    echo json_encode($photog_arr);
}
 
else{
 
    // set response code - 404 Not found
    //http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array("message" => "No photographers found.")
    );
}
?>
